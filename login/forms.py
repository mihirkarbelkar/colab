from django import forms


class SignUpForm(forms.Form):
    username = forms.CharField(label="First Name", max_length=30)
    email = forms.CharField(label="Email", max_length=30)
    password = forms.CharField(
        label="Password", widget=forms.PasswordInput, min_length=8
    )
    password_confirm = forms.CharField(
        label="Confirm Password", widget=forms.PasswordInput, min_length=8
    )


class SignInForm(forms.Form):
    username = forms.CharField(label="Username", max_length=30)
    password = forms.CharField(
        label="Password", widget=forms.PasswordInput, min_length=8
    )
