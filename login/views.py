from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import User
from django.views.decorators.csrf import csrf_exempt
from bcrypt import hashpw, gensalt
from .forms import SignInForm, SignUpForm

# Create your views here.


def index(request):
    if request.method == "POST":
        form = SignInForm(request.POST)
        if form.is_valid():
            if User.objects.filter(username=form.cleaned_data["username"].lower()):
                user = User.objects.filter(
                    username=form.cleaned_data["username"].lower()
                )[0]
                print(user.password)

            else:
                return render(
                    request, "index.html", {"message": "Not registered with us"}
                )
            if (
                user.username == form.cleaned_data["username"]
                and hashpw(
                    form.cleaned_data["password"].encode(), user.password.encode()
                )
                == user.password.encode()
            ):

                return render(request, "index.html", {"message": "Done"})
            else:
                return render(request, "index.html", {"message": "Failed to login"})
    else:
        form = SignInForm()
    return render(request, "index.html", {"form": form, "message": "not yet logged in"})


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():

            if form.cleaned_data["password"] == form.cleaned_data["password_confirm"]:
                user = User(
                    username=form.cleaned_data["username"].lower(),
                    password=hashpw(
                        form.cleaned_data["password"].encode(), gensalt()
                    ).decode(),
                    email=form.cleaned_data["email"],
                )
                user.save()
                return render(request, "index.html", {"message": "Logged in!"})

    form = SignUpForm()

    return render(request, "signup.html", {"form": form})


@csrf_exempt
def check_user(request):
    if request.method == "POST":
        if User.objects.filter(username=request.POST.get("username")):
            return True
    return False

