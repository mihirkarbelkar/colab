from django.contrib import admin
from .models import User, Key


# Register your models here.
admin.site.register(User)
admin.site.register(Key)
