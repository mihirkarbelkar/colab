from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path("", views.index, name="login_index"),
    path("signup/", views.signup, name="signup_index"),
    path("checkuser/", views.check_user, name="checkuser_api"),
]
